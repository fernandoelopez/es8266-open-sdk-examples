#include <osapi.h>
#include <c_types.h>
#include <ip_addr.h>
#include <espconn.h>
#include <driver/uart.h>
#include <user_interface.h>

os_timer_t stimer;

void ICACHE_FLASH_ATTR blink(void *arg) {
    os_printf("Mundo\n");
}

void ICACHE_FLASH_ATTR main() {
    uart_div_modify(UART0, UART_CLK_FREQ / 115200);
    os_printf("Hola\n");
}


int ICACHE_FLASH_ATTR user_init() {
    wifi_set_opmode_current(STATION_MODE);
    wifi_station_set_auto_connect(0);
    os_timer_setfn(&stimer, (os_timer_func_t *) blink, NULL);
    os_timer_arm(&stimer, 200, true);
    system_init_done_cb(main);
}
