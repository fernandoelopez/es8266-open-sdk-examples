#include <osapi.h>
#include <c_types.h>
#include <ip_addr.h>
#include <espconn.h>
#include <driver/uart.h>
#include <user_interface.h>
#include <queue.h>
#include <mem.h>

void scan_done_all(void *arg, STATUS status);
void scan_done_channel6(void *arg, STATUS status);
void scan_done_ssidlinti(void *arg, STATUS status);

void print_scan_results(struct bss_info *bss_link, STATUS status) {
    struct bss_info *old_link;

    if (status != OK) {
        os_printf("Status: %d\n", status);
        return;
    }

    while (bss_link) {
        os_printf("SSID: %s, CHAN: %u, RSSI: %d, AUTH: %d\n",
              bss_link->ssid,
              bss_link->channel,
              bss_link->rssi,
              bss_link->authmode
        );
        old_link = bss_link;
        bss_link = STAILQ_NEXT(bss_link, next);
        os_free(old_link);
    }
}

void scan_done_all(void *arg, STATUS status) {
    os_printf("Resultados para all (incluso hidden)\n");
    print_scan_results(arg, status);
    system_os_post(USER_TASK_PRIO_0, 1, 0); // Scan chan 6
}

void scan_done_channel6(void *arg, STATUS status) {
    os_printf("Resultados para el canal 6\n");
    print_scan_results(arg, status);
    system_os_post(USER_TASK_PRIO_0, 2, 0); // Scan Linti

}

void scan_done_ssidlinti(void *arg, STATUS status) {
    os_printf("Resultados para LINTI\n");
    print_scan_results(arg, status);
}

void scan_task(os_event_t *e) {
    struct scan_config all;
    struct scan_config channel6;
    struct scan_config ssidlinti;

    switch (e->sig) {
        case 0:
            os_bzero(&all, sizeof(all));
            all.show_hidden = 1;
            wifi_station_scan(&all, scan_done_all);
            break;
        case 1:
            os_bzero(&channel6, sizeof(channel6));
            channel6.channel = 6;
            wifi_station_scan(&channel6, scan_done_channel6);
            break;
        case 2:
            os_bzero(&ssidlinti, sizeof(ssidlinti));
            ssidlinti.ssid = "Linti";
            wifi_station_scan(&ssidlinti, scan_done_ssidlinti);
    }
}

void ICACHE_FLASH_ATTR main() {
    uart_div_modify(UART0, UART_CLK_FREQ / 115200);
    os_event_t *task_queue = os_malloc(sizeof(os_event_t) * 4);

    system_os_task(scan_task, USER_TASK_PRIO_0, task_queue, 4);
    system_os_post(USER_TASK_PRIO_0, 0, 0); // Scan all
}


int ICACHE_FLASH_ATTR user_init() {
    wifi_set_opmode_current(STATION_MODE);
    wifi_station_set_auto_connect(0);

    system_init_done_cb(main);
}
